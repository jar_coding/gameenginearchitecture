﻿using System.Collections;
using Settings;
using UnityEngine;

[RequireComponent(typeof(UnitMovement))]
public class UnitCombat : MonoBehaviour
{
    public float damage;
    public float attackSpeed;
    public float attackRange;
    public UnitTypeMultiplier weakness;
    
    private bool isFighting;
    private Unit thisUnit;

    private BoxCollider2D box;

    public void OnEnable() {
        thisUnit = GetComponent<Unit>();
        Reset();
        InitRangeBox();
    }

    private void InitRangeBox()
    {
        box = GetComponent<BoxCollider2D>();
        box.size = new Vector2(attackRange, .5f);
        box.offset = new Vector2(attackRange / 2, 0);
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (isFighting) {
            return;
        }
        
        Unit otherUnit = other.gameObject.GetComponent<Unit>();
        
        if (otherUnit == null) return;
        if (!otherUnit.isActiveAndEnabled || !isActiveAndEnabled) return;
        if (HasSameOwner(otherUnit.owner)) return;
        if (IsOutOfRange(otherUnit.transform)) return;

        StartFighting(otherUnit);
    }

    private bool HasSameOwner(Player otherOwner)
    {
        return thisUnit.owner == otherOwner;
    }
    
    private bool IsOutOfRange(Transform other)
    {
        return Vector3.Distance(other.position, transform.position) > attackRange;
    }
    
    private void StartFighting(Unit otherUnit)
    {
        if (!isActiveAndEnabled || isFighting) {
            return;
        }

        isFighting = true;
        thisUnit.movement.enabled = false;
        StartCoroutine(Fighting(otherUnit));
    }

    private IEnumerator Fighting(Unit otherUnit)
    {
        float dmg = GetDamage(otherUnit);

        if(!isFighting) yield break;
        
        do {
            yield return new WaitForSeconds(attackSpeed);
            
            if (otherUnit != null) {
                otherUnit.health.Reduce(dmg);
            }
            
        } while (otherUnit != null && !otherUnit.health.IsEmpty() && !IsOutOfRange(otherUnit.transform));
        
        if(otherUnit != null) {
            thisUnit.owner.AddGold(otherUnit.settings.costs);
        }
        
        Reset();
    }

    private void Reset()
    {
        isFighting = false;
        thisUnit.movement.enabled = true;
    }

    private float GetDamage(Unit otherUnit)
    {
        if (otherUnit.type == weakness.type) {
            return damage = damage * weakness.multiplier;
        }

        return damage;
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}