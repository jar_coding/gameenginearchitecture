﻿using System.Collections.Generic;
using Settings;
using UnityEngine;
using UnityEngine.UI;

public enum PlayerIdentifier { PLAYER1, PLAYER2 }

public class Player : MonoBehaviour
{
	public Text goldText;
	
	public PlayerSettings settings;
	public PlayerIdentifier identifier;
	
	private UnitSpawner spawner;
	private int gold;
	private List<Unit> units;

	private void Awake()
	{
		units = new List<Unit>();
		gold = settings.gold;
		spawner = transform.Find("Spawner").GetComponent<UnitSpawner>();
	}

	private void Start()
	{
		UpdateGoldText();
	}

	private void UpdateGoldText()
	{
		goldText.text = gold.ToString();
	}

	private void Update()
	{
		if (Input.GetButtonDown(settings.spawnSpearmanKey)) {
			spawner.Spawn(this, UnitType.SPEARMAN);
			
		} else if (Input.GetButtonDown(settings.spawnRangedKey)) {
			spawner.Spawn(this, UnitType.RANGED);
			
		} else if (Input.GetButtonDown(settings.spawnCavlryman)) {
			spawner.Spawn(this, UnitType.CAVLRYMAN);
			
		} else if (Input.GetButtonDown(settings.timeshiftKey)) {
			Timeshift();
		}
	}

	public void Save(int second)
	{
		foreach (var unit in units) {
			unit.timeData.Save(second);
		}
	}

	public bool HasEnoughGold(Unit unit)
	{
		return gold >= unit.settings.costs;
	}

	public void BuyUnit(Unit unit)
	{
		DecreaseGold(unit.settings.costs);
		units.Add(unit);
	}

	public void RefundUnit(Unit unit)
	{
		AddGold(unit.settings.costs);
		units.Remove(unit);
	}

	public void AddGold(int gold)
	{
		this.gold += gold;
		UpdateGoldText();
	}
	
	private void DecreaseGold(int gold)
	{
		this.gold -= gold;
		UpdateGoldText();
	}

	private void Timeshift()
	{
		foreach (var unit in units) {
			unit.timeData.JumpBackInTime(GameManager.currentTime - 3);
		}
	}
}
