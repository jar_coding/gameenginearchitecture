﻿using System;
using Settings;
using UnityEngine;

[RequireComponent(typeof(UnitMovement))]
[RequireComponent(typeof(UnitCombat))]
[RequireComponent(typeof(UnitHealth))]
[RequireComponent(typeof(UnitTimeData))]

public class Unit : MonoBehaviour
{
    public UnitSettings settings;
    [HideInInspector]
    public UnitType type;
    [HideInInspector]
    public Player owner;

    [HideInInspector]
    public UnitMovement movement;
    [HideInInspector]
    public UnitCombat combat;
    [HideInInspector]
    public UnitHealth health;
    [HideInInspector]
    public UnitTimeData timeData;

    private void Awake()
    {
        type = settings.type;
        SetupUnitMovement();
        SetupUnitHealth();
        SetupUnitCombat();
        SetupTimeData();
    }
    
    public void SetOwern(Player owner)
    {
        this.owner = owner;

        Vector3 direction = GetDireaction(owner.identifier);

        movement.direction = direction;
        Vector3 scale = transform.localScale;
        transform.localScale = new Vector3(direction.x * scale.y, scale.y, scale.z);
    }
    
    private void SetupUnitMovement()
    {
        movement = GetComponent<UnitMovement>();
        movement.speed = settings.movementSpeed;
    }

    private void SetupUnitHealth()
    {
        health = GetComponent<UnitHealth>();
        health.health = settings.maximumHealth;
    }
    
    private void SetupUnitCombat()
    {
        combat = GetComponent<UnitCombat>();
        combat.attackSpeed = settings.attackSpeed;
        combat.damage = settings.damage;
        combat.attackRange = settings.attackRange;
        combat.weakness = settings.weakness;
    }

    private void SetupTimeData()
    {
        timeData = GetComponent<UnitTimeData>();
    }

    private static Vector3 GetDireaction(PlayerIdentifier identifier)
    {
        return identifier == PlayerIdentifier.PLAYER1 ? Vector3.right : Vector3.left;
    }
    
    private void Update()
    {
        DestroyIfOutOfBound();
    }
    
    private void DestroyIfOutOfBound()
    {
        // TODO:Remove Magic Numbers
        if (transform.localPosition.x > 13f || transform.localPosition.x < -13f) {
            gameObject.SetActive(false);
        }
    }

    private void OnDestroy()
    {
        owner.RefundUnit(this);
    }
}