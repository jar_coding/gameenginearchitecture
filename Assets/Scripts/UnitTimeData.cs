﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(UnitHealth))]
public class UnitTimeData : MonoBehaviour
{
    private Dictionary<int, TimeData> timeData;
    private UnitHealth unitHealth;

    private void Start()
    {
        unitHealth = GetComponent<UnitHealth>();
    }

    private void OnEnable()
    {
        timeData = new Dictionary<int, TimeData>();
    }
    
    public void Save(int second)
    {
        TimeData unitTimeData = new TimeData {
            health = unitHealth.health,
            position = transform.position
        };
        
        timeData.Add(second, unitTimeData);
    }

    public void JumpBackInTime(int second)
    {
        TimeData unitTimeData;

        if (timeData.TryGetValue(second, out unitTimeData)) {
            transform.position = unitTimeData.position;
            unitHealth.health = unitTimeData.health;
            gameObject.SetActive(true);
        } else {
            //unitOwner.owner.RefundUnit(this);
        }
    }
}

public struct TimeData
{
    public Vector3 position;
    public int health;
}