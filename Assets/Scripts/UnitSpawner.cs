﻿using System;
using UnityEngine;

public class UnitSpawner : MonoBehaviour
{
    public Unit Spawn(Player owner, UnitType type)
    {
        switch (type) {
            case UnitType.SPEARMAN:
                return InstantiateUnitIfPlayerHasEnoughGold(Prefabs.Spearman, owner);
            case UnitType.CAVLRYMAN:
                return InstantiateUnitIfPlayerHasEnoughGold(Prefabs.Cavlryman, owner);
            case UnitType.RANGED:
                return InstantiateUnitIfPlayerHasEnoughGold(Prefabs.Ranged, owner);
            default:
                throw new ArgumentOutOfRangeException("type", type, null);
        }
    }

    private Unit InstantiateUnitIfPlayerHasEnoughGold(Unit prefab, Player owner)
    {
        if (!owner.HasEnoughGold(prefab)) return null;

        Unit instance = Instantiate(prefab, transform);
        instance.SetOwern(owner);
        
        owner.BuyUnit(instance);

        return instance;
    }
}