﻿using UnityEngine;

namespace Settings
{
    [CreateAssetMenu(menuName = "Settings/GameSettings")]
    public class GameSettings : ScriptableObject
    {
        public int maximumJumpBackTime = 180;
    }
}
