﻿using UnityEngine;

namespace Settings
{
    [CreateAssetMenu(menuName = "Settings/UnitSettings")]
    public class UnitSettings : ScriptableObject
    {
        public UnitType type;
        public UnitTypeMultiplier weakness;
        //public UnitTypeMultiplier strengths;

        [Header("Properties")]
        public int costs = 10;
        public int maximumHealth;
        public int damage;
        public float attackRange;
        public float attackSpeed;
        public float movementSpeed;
    }

    [System.Serializable]
    public class UnitTypeMultiplier
    {
        public UnitType type;
        public float multiplier;
    }
}