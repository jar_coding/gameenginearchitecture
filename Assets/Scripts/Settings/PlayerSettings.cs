﻿using UnityEngine;

namespace Settings
{
    [CreateAssetMenu(menuName = "Settings/PlayerSettings")]
    public class PlayerSettings : ScriptableObject
    {
        [Header("Start Properties")]
        public int maximumTimeshifts;
        public int health = 100;
        public int gold = 1000;

        [Header("Keys")]
        public string spawnSpearmanKey;
        public string spawnCavlryman;
        public string spawnRangedKey;
        public string timeshiftKey;
    }
}