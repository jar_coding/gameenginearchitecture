﻿using UnityEngine;

public abstract class Prefabs
{
    public static readonly Unit Spearman = Resources.Load<Unit>("Prefabs/Spearman");
    public static readonly Unit Cavlryman = Resources.Load<Unit>("Prefabs/Cavlryman");
    public static readonly Unit Ranged = Resources.Load<Unit>("Prefabs/Ranged");
}