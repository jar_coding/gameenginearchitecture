﻿using UnityEngine;

public class UnitHealth : MonoBehaviour
{
    public int health;
    
    public bool IsEmpty()
    {
        return health <= 0;
    }

    public void Reduce(float damage)
    {
        int damageInt = Mathf.RoundToInt(damage);

        health -= damageInt;

        if (damage >= health) {
            Die();
        }
    }

    private void Die()
    {
        // TODO: DieAnimation
        
        gameObject.SetActive(false);
    }
}