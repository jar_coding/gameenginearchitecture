﻿using System.Collections;
using Settings;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static int currentTime;
    
    public GameSettings settings;

    private Player[] players;

    private void Start()
    {
        InstantiatePlayers();
        StartCoroutine(SaveGameState());
    }

    private void InstantiatePlayers()
    {
        players = FindObjectsOfType<Player>();
    }

    private IEnumerator SaveGameState()
    {
        while (true) {
            currentTime++;
            yield return new WaitForSeconds(1);
            
            foreach(var player in players)
            {
                player.Save(currentTime);
            }
        }
    }
}
