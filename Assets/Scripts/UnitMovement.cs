﻿using UnityEngine;

public class UnitMovement : MonoBehaviour
{
	public Vector3 direction;
	public float speed;
	
	private void Update ()
	{
		float x = direction.x * speed * Time.deltaTime / 16f;
		transform.Translate(x, 0, 0);
	}
}
